package main

import (
	"log"

	"github.com/prometheus/client_golang/prometheus"
)

type collector struct {
	apiKey string
}

func (m collector) Describe(ch chan<- *prometheus.Desc) {
	ch <- sent
	ch <- hardBounces
	ch <- softBounces
	ch <- rejects
	ch <- complaints
	ch <- unsubs
	ch <- opens
	ch <- clicks
	ch <- unique_opens
	ch <- unique_clicks
}

type mandrillUserInfo struct {
	Username string `json:"username"`
	Stats    struct {
		AllTime struct {
			Sent          int `json:"sent"`
			SoftBounces   int `json:"soft_bounces"`
			HardBounces   int `json:"hard_bounces"`
			Rejects       int `json:"rejects"`
			Complaints    int `json:"complaints"`
			Unsubs        int `json:"unsubs"`
			Opens         int `json:"opens"`
			Clicks        int `json:"clicks"`
			Unique_opens  int `json:"unique_opens"`
			Unique_clicks int `json:"unique_clicks"`
		} `json:"all_time"`
		Days7 struct {
			Sent          int `json:"sent"`
			SoftBounces   int `json:"soft_bounces"`
			HardBounces   int `json:"hard_bounces"`
			Rejects       int `json:"rejects"`
			Complaints    int `json:"complaints"`
			Unsubs        int `json:"unsubs"`
			Opens         int `json:"opens"`
			Clicks        int `json:"clicks"`
			Unique_opens  int `json:"unique_opens"`
			Unique_clicks int `json:"unique_clicks"`
		} `json:"last_7_days"`
		Days30 struct {
			Sent          int `json:"sent"`
			SoftBounces   int `json:"soft_bounces"`
			HardBounces   int `json:"hard_bounces"`
			Rejects       int `json:"rejects"`
			Complaints    int `json:"complaints"`
			Unsubs        int `json:"unsubs"`
			Opens         int `json:"opens"`
			Clicks        int `json:"clicks"`
			Unique_opens  int `json:"unique_opens"`
			Unique_clicks int `json:"unique_clicks"`
		} `json:"last_30_days"`
		Days60 struct {
			Sent          int `json:"sent"`
			SoftBounces   int `json:"soft_bounces"`
			HardBounces   int `json:"hard_bounces"`
			Rejects       int `json:"rejects"`
			Complaints    int `json:"complaints"`
			Unsubs        int `json:"unsubs"`
			Opens         int `json:"opens"`
			Clicks        int `json:"clicks"`
			Unique_opens  int `json:"unique_opens"`
			Unique_clicks int `json:"unique_clicks"`
		} `json:"last_60_days"`
		Days90 struct {
			Sent          int `json:"sent"`
			SoftBounces   int `json:"soft_bounces"`
			HardBounces   int `json:"hard_bounces"`
			Rejects       int `json:"rejects"`
			Complaints    int `json:"complaints"`
			Unsubs        int `json:"unsubs"`
			Opens         int `json:"opens"`
			Clicks        int `json:"clicks"`
			Unique_opens  int `json:"unique_opens"`
			Unique_clicks int `json:"unique_clicks"`
		} `json:"last_90_days"`
	}
}

func (m collector) Collect(ch chan<- prometheus.Metric) {
	info, err := getMandrillUserInfo(m.apiKey)
	if err != nil {
		log.Println(err)
		return
	}

	stats := info.Stats.AllTime
	stats7 := info.Stats.Days7
	stats30 := info.Stats.Days30
	stats60 := info.Stats.Days60
	stats90 := info.Stats.Days90
	ch <- prometheus.MustNewConstMetric(sent, prometheus.CounterValue, float64(stats.Sent), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(hardBounces, prometheus.CounterValue, float64(stats.HardBounces), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(softBounces, prometheus.CounterValue, float64(stats.SoftBounces), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(rejects, prometheus.CounterValue, float64(stats.Rejects), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(complaints, prometheus.CounterValue, float64(stats.Complaints), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(unsubs, prometheus.CounterValue, float64(stats.Unsubs), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(opens, prometheus.CounterValue, float64(stats.Opens), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(clicks, prometheus.CounterValue, float64(stats.Clicks), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(unique_opens, prometheus.CounterValue, float64(stats.Unique_opens), info.Username, "AllTime")
	ch <- prometheus.MustNewConstMetric(unique_clicks, prometheus.CounterValue, float64(stats.Unique_clicks), info.Username, "AllTime")

	ch <- prometheus.MustNewConstMetric(sent, prometheus.CounterValue, float64(stats7.Sent), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(hardBounces, prometheus.CounterValue, float64(stats7.HardBounces), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(softBounces, prometheus.CounterValue, float64(stats7.SoftBounces), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(rejects, prometheus.CounterValue, float64(stats7.Rejects), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(complaints, prometheus.CounterValue, float64(stats7.Complaints), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(unsubs, prometheus.CounterValue, float64(stats7.Unsubs), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(opens, prometheus.CounterValue, float64(stats7.Opens), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(clicks, prometheus.CounterValue, float64(stats7.Clicks), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(unique_opens, prometheus.CounterValue, float64(stats7.Unique_opens), info.Username, "7Days")
	ch <- prometheus.MustNewConstMetric(unique_clicks, prometheus.CounterValue, float64(stats7.Unique_clicks), info.Username, "7Days")

	ch <- prometheus.MustNewConstMetric(sent, prometheus.CounterValue, float64(stats30.Sent), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(hardBounces, prometheus.CounterValue, float64(stats30.HardBounces), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(softBounces, prometheus.CounterValue, float64(stats30.SoftBounces), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(rejects, prometheus.CounterValue, float64(stats30.Rejects), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(complaints, prometheus.CounterValue, float64(stats30.Complaints), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(unsubs, prometheus.CounterValue, float64(stats30.Unsubs), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(opens, prometheus.CounterValue, float64(stats30.Opens), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(clicks, prometheus.CounterValue, float64(stats30.Clicks), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(unique_opens, prometheus.CounterValue, float64(stats30.Unique_opens), info.Username, "30Days")
	ch <- prometheus.MustNewConstMetric(unique_clicks, prometheus.CounterValue, float64(stats30.Unique_clicks), info.Username, "30Days")

	ch <- prometheus.MustNewConstMetric(sent, prometheus.CounterValue, float64(stats60.Sent), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(hardBounces, prometheus.CounterValue, float64(stats60.HardBounces), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(softBounces, prometheus.CounterValue, float64(stats60.SoftBounces), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(rejects, prometheus.CounterValue, float64(stats60.Rejects), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(complaints, prometheus.CounterValue, float64(stats60.Complaints), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(unsubs, prometheus.CounterValue, float64(stats60.Unsubs), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(opens, prometheus.CounterValue, float64(stats60.Opens), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(clicks, prometheus.CounterValue, float64(stats60.Clicks), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(unique_opens, prometheus.CounterValue, float64(stats60.Unique_opens), info.Username, "60Days")
	ch <- prometheus.MustNewConstMetric(unique_clicks, prometheus.CounterValue, float64(stats60.Unique_clicks), info.Username, "60Days")

	ch <- prometheus.MustNewConstMetric(sent, prometheus.CounterValue, float64(stats90.Sent), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(hardBounces, prometheus.CounterValue, float64(stats90.HardBounces), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(softBounces, prometheus.CounterValue, float64(stats90.SoftBounces), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(rejects, prometheus.CounterValue, float64(stats90.Rejects), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(complaints, prometheus.CounterValue, float64(stats90.Complaints), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(unsubs, prometheus.CounterValue, float64(stats90.Unsubs), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(opens, prometheus.CounterValue, float64(stats90.Opens), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(clicks, prometheus.CounterValue, float64(stats90.Clicks), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(unique_opens, prometheus.CounterValue, float64(stats90.Unique_opens), info.Username, "90Days")
	ch <- prometheus.MustNewConstMetric(unique_clicks, prometheus.CounterValue, float64(stats90.Unique_clicks), info.Username, "90Days")
}
