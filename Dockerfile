FROM alpine:latest

RUN apk --no-cache add ca-certificates
RUN apk add --no-cache libc6-compat

COPY ./mandrill-prometheus-exporter .
ENTRYPOINT ["./mandrill-prometheus-exporter"]

EXPOSE 9861